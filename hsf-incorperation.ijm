// %age plate v1.7

macro "%age Plate - C0aL [f12]"{

    //set the scale for 3k magnification
    run("Set Scale...", "distance=22.667 known=1 pixel=1 unit=�m global");

    // find working directory
    dir = getDirectory("image");

    // up one level and locate dedicated ImageJ directory
    imagejfolder = replace(dir,"Micrographs","ImageJ Analysis");

    //generate full path and filename
    name=getTitle;
    path = imagejfolder+name;

    //creating filenames for each saved image and spreadsheet
    crop = replace(path,".tif", "_1.tif");
    binary = replace(path,".tif", "_2.tif");
    map = replace(path,".tif", "_3.tif");
    summary = replace(path,".tif", "_Summary.xls");
    results = replace(path,".tif", "_Results.xls");

    //cropped image
    saveAs("Tiff", crop);

    //binary image
    run("Make Binary");
    saveAs("Tiff", binary);
    mapname=getTitle;

    //set measurements
    run("Set Measurements...", "area mean standard modal min centroid center perimeter bounding fit shape feret's integrated median skewness kurtosis area_fraction stack redirect=None decimal=3");

    //particle analysis
    run("Analyze Particles...", "size=0.01-Infinity circularity=0.00-1.00 show=Outlines display clear include summarize record in_situ");
    
    //results needs to be saved and closed
    selectWindow("Results");
    saveAs("Results", results);
    run("Close");

    selectWindow(mapname);
    saveAs("Tiff", map);

    //open next image
    run("Open Next");
}
